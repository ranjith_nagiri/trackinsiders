﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TrackInsider.Web.Startup))]
namespace TrackInsider.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
